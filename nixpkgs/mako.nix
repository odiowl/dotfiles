{ config, ... }:
{
  enable = true;

  borderRadius = 3;
  borderSize = 2;
  layer = "overlay";
  anchor = "bottom-right";
  sort = "-time";
  padding = "7";
  width = 400;
  font = "Iosevka Aile Light 12";

  extraConfig = ''
    [urgency=low]
    background-color=#ababab
    border-color=#888888
    text-color=#000000
    default-timeout=2000

    [urgency=normal]
    background-color=#30517f
    border-color=#00538b
    text-color=#ffffff
    default-timeout=5000

    [urgency=high]
    background-color=#a60000
    border-color=#972500
    text-color=#ffffff
    default-timeout=0
  '';
}
