{ config, pkgs, ... }:

# TODO's:
#   - desktop application for chromium not found - it's already in $HOME/.nix-profile/share/applications!

{
  # TODO: activate browserpass for testing
  programs.browserpass = {
   enable = true;
   browsers = [ "chromium" "firefox" ];
  };

  # for starting in native wayland system call it with
  # chromium --enable-features=UseOzonePlatform --ozone-platform=wayland
  programs.chromium = {
    enable = true;
    commandLineArgs = [
      "--enable-features=UseOzonePlatform"
      "--ozone-platform=wayland"
    ];
    # extension-id's are from the url of the web-store
    extensions = [
      "naepdomgkenhinolocfifgehidddafch" # browserpass
      "cfhdojbkjhnklbpkdaibdccddilifddb" # ad block plus
      "dbepggeogbaibhgnhhndojpepiihcmeb" # Vimium
    ];
  };

}
