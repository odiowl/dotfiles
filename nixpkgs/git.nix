{ config, pkgs, ... }:

{
  programs.git = {
    enable = true;
    package = pkgs.gitAndTools.gitFull;

    # default Name, email will be set in the projects itself
    userName = "Oliver Dunkl";

    aliases = {
      co = "checkout";
      s = "status";
      b = "branch";
    };

    extraConfig = {
      github = {
        user = "odi";
      };
      gitlab = {
        user = "odiowl";
      };
      merge = {
        conflictstyle = "diff3";
      };
    };
  };

  home.packages = with pkgs; [
    git-secret
    git-crypt
  ];
  
}
