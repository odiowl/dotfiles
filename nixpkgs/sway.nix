{ config, pkgs, lib, ... }:

# Icons and all-the-icons integrations
#  - https://atom.io/packages/file-icons
#  - https://github.com/domtronn/all-the-icons.el/tree/master/data
#  - https://github.com/domtronn/all-the-icons.el
#
# Get Backgrounds:
#  - https://unsplash.com/

let
  swayModeTheme = "theme: l ... light, d ... dark";
  monitorInternal = "eDP-1";
  monitorExternal1 = "DP-1";
  monitorExternal2 = "DP-2";
  lockScreenImage = "$HOME/Pictures/Wallpapers/wallpaper-37.jpg";
  terminalBin = "${config.programs.alacritty.package}/bin/alacritty";
  rofiBin = "${config.programs.rofi.package}/bin/rofi";
  emacsClientBin = "${config.programs.emacs.package}/bin/emacsclient";
  swayInnerGapsSize = 5;
  swayModeEnvironmentPrompt = "ENVIRONMENT: (n) notebook (h) home";
  swayModeThemePrompt = "THEME: (d) dark (l) light";
in
{

  # packages only useful inside sway
  home.packages = with pkgs; [
    sway-contrib.grimshot
    slurp
    libappindicator
    wl-clipboard
    brightnessctl
    wdisplays
    swaylock
    chafa
  ];

  wayland.windowManager.sway = {
    enable = true;
    config = {
      bars = []; # I use waybar with systemd-unit
      modifier = "Mod4";
      defaultWorkspace = "1";
      focus.newWindow = "smart";

      gaps = {
        inner = swayInnerGapsSize;
        smartBorders = "on";
        smartGaps = false;
      };

      fonts = {
        names = [ "Fira Sans" ];
        size = 10.0;
      };

      floating = {
        modifier = config.wayland.windowManager.sway.config.modifier;
      };

      input = {
        "type:touchpad" = {
          tap = "enabled";
          natural_scroll = "enabled";
        };
        "type:keyboard" = {
          xkb_layout = "us";
          xkb_variant = "altgr-intl";
          xkb_options = "compose:caps";
        };
      };

      modes = {
        SYSTEM = {
          j = "exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -2%";
          k = "exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +2%";
          m = "exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle";
          u = "exec --no-startup-id ${pkgs.brightnessctl}/bin/brightnessctl set 2%-";
          i = "exec --no-startup-id ${pkgs.brightnessctl}/bin/brightnessctl set +2%";
          s = "exec $HOME/.config/sway/scripts/screenshot.sh, mode default";
          "shift+s" = "exec --no-startup-id systemctl suspend, mode default";
          "shift+l" = "exec --no-startup-id swaylock -i ${lockScreenImage} -f, mode default";
          Return = "mode default";
          Escape = "mode default";
        };

        RESIZE = {
          h = "resize shrink width 2 px or 2 ppt";
          j = "resize grow height 2 px or 2 ppt";
          k = "resize shrink height 2 px or 2 ppt";
          l = "resize grow width 2 px or 2 ppt";
          Left = "resize shrink width 5 px or 5 ppt";
          Down = "resize grow height 5 px or 5 ppt";
          Up = "resize shrink height 5 px or 5 ppt";
          Right = "resize grow width 5 px or 5 ppt";
          "Shift+Left" = "move left 10 px";
          "Shift+Right" = "move right 10 px";
          "Shift+Up" = "move up 10 px";
          "Shift+Down" = "move down 10 px";
          c = "move position center";
          m = "floating enable, resize set height 100ppt, resize set width 100ppt, move position 0 0, mark --toggle MAX";
          "Shift+m" = "floating disable, mark --toggle MAX, mode default";
          g = "gaps inner current set 5";
          "Shift+g" = "gaps inner current set 0";
          Return = "mode default";
          Escape = "mode default";
        };

        ${swayModeThemePrompt} = {
          l = "exec --no-startup-id gsettings set org.gnome.desktop.interface color-scheme prefer-light, mode default";
          d = "exec --no-startup-id gsettings set org.gnome.desktop.interface color-scheme prefer-dark, mode default";
          Return = "mode default";
          Escape = "mode default";
        };

        ${swayModeEnvironmentPrompt} = {
          n = "output ${monitorInternal} enable mode 1920x1080 position 0 0, output ${monitorExternal1} disable, output ${monitorExternal2} disable, mode default";
          h = "output ${monitorInternal} enable mode 1920x1080 position 1270 1680, output ${monitorExternal1} enable mode 2560x1440 position 1080 240, output ${monitorExternal2} enable mode 1920x1080 position 0 0 transform 90, mode default";
          Return = "mode default";
          Escape = "mode default";
        };
      };

      keybindings =
        let
          mod = config.wayland.windowManager.sway.config.modifier;
        in lib.mkOptionDefault {
          "${mod}+Return" = "exec ${terminalBin}";
          "${mod}+d" = "exec ${rofiBin} -show drun -show-icons";
          # rofi-pass produces error message which relates to wrong usage of rofi
          # hence I use 2>/dev/null to suppress this message
          "${mod}+y" = "exec ${pkgs.rofi-pass}/bin/rofi-pass 2>/dev/null";
          # use the same as in emacs.nix
          "${mod}+c" = "exec ${emacsClientBin} -c -a 'emacs'";
          "${mod}+n" = "workspace next_on_output";
          "${mod}+p" = "workspace prev_on_output";
          # switch to next/prev workspace with $mod+Mousewheel (like in Gnome)
          "--whole-window --border ${mod}+button4" = "workspace prev_on_output";
          "--whole-window --border ${mod}+button5" = "workspace next_on_output";
          "${mod}+b" = "exec pkill -SIGUSR1 waybar";
          "${mod}+q" = "kill";
          "${mod}+Shift+y" = "mode SYSTEM";
          "${mod}+Shift+r" = "mode RESIZE";
          "${mod}+Shift+t" = "mode \"${swayModeThemePrompt}\"";
          "${mod}+Shift+e" = "mode \"${swayModeEnvironmentPrompt}\"";
          "${mod}+Shift+s" = "sticky toggle, mark --toggle STICKY";
          "${mod}+Period" = "scratchpad show";
          "${mod}+Ctrl+Period" = "move scratchpad";
          "${mod}+Ctrl+c" = "[app_id=org.gnome.Calendar] scratchpad show, move position center";
          "${mod}+Ctrl+r" = "[title=PyRadio.*] scratchpad show, move position center";
          "${mod}+Ctrl+k" = "[app_id=org.keepassxc.KeePassXC] scratchpad show, move position center";
          "${mod}+Ctrl+f" = "exec ~/.nix-profile/bin/fzf-file-open.sh";
          "${mod}+Ctrl+s" = "exec ~/.nix-profile/bin/screenshot.sh";
        };

      window = {
        titlebar = false;
        commands = [
          { criteria = { app_id = "org.gnome.Calendar"; }; command = "floating enable"; }
          { criteria = { app_id = "gcolor3"; }; command = "floating enable"; } # color picker
        ];
      };

      startup = [
        { command = "systemctl --user restart waybar"; always = true; }
        { command = "systemctl --user restart kanshi"; always = true; }
        { command = "nm-applet --indicator"; }
        # turn on light for notebook keyboard
        { command = "${pkgs.brightnessctl}/bin/brightnessctl -d dell::kbd_backlight set 2"; }
      ];

      workspaceOutputAssign = [
        { workspace = "1"; output = "${monitorExternal1} ${monitorInternal}"; }
        { workspace = "2"; output = "${monitorExternal1} ${monitorInternal}"; }
        { workspace = "8"; output = "${monitorExternal2} ${monitorInternal}"; }
        { workspace = "9"; output = "${monitorInternal}"; }
      ];

    };
  };

  # dynamic output management for wayland
  services.kanshi = {
    enable = true;
    profiles = {
      undocked = {
        outputs = [
          {
            criteria = "${monitorInternal}";
            position = "0,0";
          }
        ];
        exec = [
          "${pkgs.sway}/bin/sway output '*' bg '#444444' solid_color"
          "${pkgs.libnotify}/bin/notify-send --urgency=low 'Kanshi changed profile' 'Changed profile to undocked'"
        ];
      };
      # get configuration with `wdisplays`
      docked = {
        outputs = [
          {
            criteria = "${monitorInternal}";
            position = "0,1619";
            mode = "1920x1080";
          }
          {
            criteria = "${monitorExternal1}";
            position = "410,179";
            mode = "2560x1440";
          }
          {
            criteria = "${monitorExternal2}";
            position = "2970,0";
            mode = "1920x1080";
            transform = "90";
          }
        ];
        exec = [
          "${pkgs.sway}/bin/sway output '*' bg '#444444' solid_color"
          "${pkgs.libnotify}/bin/notify-send --urgency=low 'Kanshi changed profile' 'Changed profile to docked'"
          "systemctl --user restart waybar"
        ];
      };
    };
  };

  # -- Waybar
  # https://github.com/Alexays/Waybar
  programs.waybar = {
    enable = true;
    systemd.enable = true;
    settings = {
      mainBar = {
        layer = "bottom";
        position = "bottom";
        output = ["${monitorInternal}" "${monitorExternal1}" "${monitorExternal2}"];
        height = 27;
        margin = "0";
        modules-left = ["sway/workspaces" "sway/mode" "sway/window"];
        modules-right = [ "backlight"
                          "custom/disk-root-pref" "disk#root" "custom/disk-home-pref" "disk#home"
                          "custom/memory-pref" "memory"
                          "pulseaudio#pref" "pulseaudio"
		                      "battery#pref" "battery" "custom/gammastep" "tray" "clock#date" "clock#time"
                        ];
        "sway/workspaces" = {
	        all-outputs = false;
        };
        "sway/window" = {
          format = "{}";
          max-length = 50;
        };
        "battery" = {
	        format = "{capacity}%";
	        format-full = "";
	        format-discharging = "🡓{capacity}%";
	        format-charging = "🡑{capacity}%";
	        format-plugged = "{capacity}%";
	        states = {
            warning = 25;
	          critical = 15;
	        };
        };
        "battery#pref" = {
	        format = "";
	        format-full = "";
	        format-discharging = "{icon}";
	        format-charging = "{icon}";
	        format-plugged = "";
          format-icons = ["" "" "" "" "" "" "" "" "" ""];
	        states = {
            warning = 25;
	          critical = 15;
	        };
        };
        "clock#date" = {
	        format = "{:%a %d %b}";
	        timezones = ["Europe/Vienna"];
	        tooltip = false;
	        on-click-right = "";
        };
        "clock#time" = {
	        format = "{:%H:%M}";
	        timezones = ["Europe/Vienna"];
	        tooltip = false;
	        on-click-right = "";
        };
        "tray" = {
          icon-size = 14;
          spacing = 8;
        };
        "pulseaudio" = {
          format = "{volume}%";
          format-bluetooth = "{volume}% ";
          format-muted = "MUTED";
          on-click = "pactl set-sink-mute @DEFAULT_SINK@ toggle";
          on-click-right = "pavucontrol";
        };
        "pulseaudio#source" = {
          format = "{format_source}";
          format-source = "";
          format-source-muted = "";
        };
        "pulseaudio#pref" = {
          format = "墳";
          format-bluetooth = "墳";
          format-muted = "ﱝ";
          on-click = "pactl set-sink-mute @DEFAULT_SINK@ toggle";
          on-click-right = "pavucontrol";
        };
        "memory" = {
          interval = 30;
          format = " {percentage}%";
          tooltip-format = "{used:0.1f}GiB ({percentage}%) used/{swapUsed:0.1f}GiB ({swapPercentage}%) swap";
          states = {
            warning = 85;
            critical = 95;
          };
        };
        "custom/memory-pref" = {
          format = "";
          tooltip = false;
        };
        "backlight" = {
          device = "intel_backlight";
          format = " {percent}%";
        };
        "custom/gammastep" = {
          format = "{icon}";
          return-type = "json";
          interval = 5;
          exec-if = "$HOME/.local/share/bin/waybar/gammastep.sh check";
          exec = "$HOME/.local/share/bin/waybar/gammastep.sh";
          format-icons = {
            gammastep = "";
          };
        };
        "disk#root" = {
          interval = 30;
          path = "/";
          format = "{percentage_used}%";
          states = {
            warning = 85;
            critical = 95;
          };
        };
        "custom/disk-root-pref" = {
          format = "";
          tooltip = false;
        };
        "disk#home" = {
          interval = 30;
          path = "/home";
          format = "{percentage_used}%";
          states = {
            warning = 85;
            critical = 95;
          };
        };
        "custom/disk-home-pref" = {
          format = "";
          tooltip = false;
        };
      };
    };
    style = ''
        * {
          border: none;
          border-radius: 0;
          font-family: Iosevka Aile;
          font-size: 14px;
          min-height: 0;
          padding: 0px;
        }

        window#waybar {
          background: #222222;
          color: #cccccc;
        }

        tooltip {
          background: #333333;
          border: 1px solid #444444;
          color: white;
          border-radius: 1px;
          font-size: 20px;
        }

        tooltip label {
          color: white;
          font-size: 14px;
          font-family: DroidSans;
        }

        #workspaces button {
          padding-left: 3px;
          padding-right: 3px;
          background: #541f4f;
          border: 0px;
          border: 1.5px solid #752f50;
          color: #aaaaaa;
          margin-bottom: 2px;
          margin-top: 2px;
          padding-top: 2px;
          border-radius: 1.5px;
          margin-right: 1px;
          margin-left: 1px;
          font-weight: normal;
        }

        #workspaces button.current_output {
          background: #333333;
          border: 1.5px solid #444444;
          border-radius: 1px;
        }

        #workspaces button.visible {
          background: #813d9c;
          color: #ffffff;
          border: 1.5px solid #c061cb;
          border-radius: 1px;
        }

        #workspaces button.focused {
          background: #285577;
          color: #ffffff;
          border: 1.5px solid #4c7899;
          border-radius: 1px;
          font-weight: bold;
        }

        #workspaces button.urgent {
          background: #a60000;
          color: white;
          border: 1px solid #c33838;
        }

        #window {
          margin-left: 10px;
          font-weight: normal;
        }

        #mode {
          padding-left: 3px;
          padding-right: 3px;
          margin-left: 2px;
          background: #eecc00;
          color: #333333;
          border: 1px solid #f0ce43;
          margin-bottom: 2px;
          margin-top: 2px;
          border-radius: 1px;
        }

        #clock.date {
          font-weight: normal;
          margin-left: 10px;
        }

        #clock.time {
          font-weight: bold;
          margin-left: 8px;
          margin-right: 8px;
        }

        #tray {
          margin-left: 8px;
          font-size: 20px;
        }

        #battery {
        }

        #battery.full {
        }

        #battery.warning {
          color: #eecc00;
        }

        #battery.critical {
          color: #c01c28;
        }

        #battery.pref {
          font-size: 9pt;
          margin-left: 8px;
          padding-bottom: 2px;
        }

        #pulseaudio {
          margin-right: 3px;
        }

        #pulseaudio.bluetooth {
          color: #29aeff;
        }

        #pulseaudio.muted {
          color: #666666;
        }

        #pulseaudio.pref {
          margin-left: 8px;
          font-size: 16pt;
        }

        #memory.warning {
          color: #f5c211;
        }

        #memory.critical {
          color: #c01c28;
        }

        #custom-memory-pref {
          font-size: 16pt;
          margin-left: 8px;
        }

        #backlight {
          margin-left: 3px;
          margin-right: 3px;
        }

        #custom-gammastep {
          font-size: 14pt;
          margin-left: 10px;
        }

        #disk {
          margin-left: 3px;
          margin-right: 3px;
        }

        #disk.warning {
          color: #f5c211;
        }

        #disk.critical {
          color: #c01c28;
        }

        #custom-disk-home-pref {
          font-size: 14pt;
          margin-left: 8px;
        }

        #custom-disk-root-pref {
          font-size: 14pt;
          margin-left: 8px;
        }

        #custom-delimiter {
          font-size: 16pt;
          color: #4a708b;
          margin-right: 5px;
          margin-left: 5px;
        }
    '';
  };

  programs.mako = import ./mako.nix { inherit config; };
  programs.rofi = import ./rofi.nix { inherit config; inherit pkgs; };
  

  # Script for showing vpn informations in waybar
  xdg.dataFile."bin/waybar/vpn.sh" = {
    executable = true;
    # TODO: change nmcli/grep to nixpkgs version
    text = ''
         #!${pkgs.bash}/bin/bash

         VPN_NAME=$(nmcli con show --active | grep vpn | awk '{print $1}')
         echo '{"text":"'$VPN_NAME'","tooltip":" '$VPN_NAME' ","alt":"'$VPN_NAME'"}'
    '';
  };

  # -- Gammastep
  # https://gitlab.com/chinstrap/gammastep
  services.gammastep = {
    enable = true;
    latitude = "48.2";
    longitude = "16.37";
    provider = "manual";
    temperature = {
      day = 6500;
      night = 4500;
    };
    settings = {
      general = {
        gamma = "1.0";
        adjustment-method = "wayland";
        fade = 1;
      };
    };
  };

  # Script for getting information about gammastep
  xdg.dataFile."bin/waybar/gammastep.sh" = {
    executable = true;
    # TODO: change grep/pgrep to nixpkgs version
    text = ''
         #!${pkgs.bash}/bin/bash

         state=$(${pkgs.gammastep}/bin/gammastep -p 2>&1 | grep Period | cut -f3 -d" ")
         temp=$(${pkgs.gammastep}/bin/gammastep -p 2>&1 | grep temperature | cut -f4 -d" ")
         if [ "$1" == "check" ]; then
            pgrep gammastep && test "Night" = "$state" -o "Transition" = "$state"
         else
            echo '{"alt": "gammastep", "tooltip": "Period: '$state', Color Temperatur: '$temp'"}'
         fi
    '';
  };

  # -- Swayidle
  # after 300s lock the screen
  # after 600s turn off all screens
  # after resume turn on screens
  # before-sleep lock screen
  services.swayidle = {
    enable = true;
    timeouts = [
      { timeout = 300; command = "swaylock -i ${lockScreenImage} -f"; }
      #{ timeout = 600; command = "swaymsg 'output * dpms off'"; }
    ];
    events = [
      { event = "before-sleep"; command = "swaylock -i ${lockScreenImage} -f"; }
      #{ event = "after-resume"; command = "swaymsg 'output * dpms on'"; }
    ];
  };
  
}
