{ config, pkgs, ...}:
{
  enable = true;
  package = pkgs.rofi-wayland.override { plugins = [ pkgs.rofi-emoji ]; };
  font = "Iosevka SS12 14";
  terminal = "${config.programs.alacritty.package}/bin/alacritty";
  extraConfig = {
    modi = "drun,run,ssh";
    matching = "normal";
    combi-mode = "drun,run";
  };

  theme = let
    inherit (config.lib.formats.rasi) mkLiteral;
  in {
    "@theme" = "default"; # load default theme, get it with $ rofi -dump-theme
    # overwrite some defaults ...
    "*" = {
      background = mkLiteral "#303030";
      lightbg = mkLiteral "#888888";
      alternate-normal-background = mkLiteral "#383838";
      alternate-active-background = mkLiteral "#383838";
      selected-normal-background = mkLiteral "#752f50";
      foreground = mkLiteral "#e0e0e0";
      selected-normal-foreground = mkLiteral "#ffffff";
      border-color = mkLiteral "#e0b2d6";
      separatorcolor = mkLiteral "#555555";
    };
    "window" = {
      border = 3;
      background-color = mkLiteral "var(background)";
    };
    "scrollbar" = {
      handle-color = mkLiteral "#e0b2d6";
      handle-width = mkLiteral "2px";
    };
    "element-text" = {
      highlight = mkLiteral "bold underline #eecc00";
    };
    "prompt" = {
      text-color = mkLiteral "#feacd0";
    };
    "textbox-prompt-colon" = {
      text-color = mkLiteral "#feacd0";
    };
    "listview" = {
      dynamic = false; # does not work right
      fixed-height = false;
    };
  };

  pass = {
    enable = true;
    stores = [ "${config.programs.password-store.settings.PASSWORD_STORE_DIR}" ];
    # example config:
    # https://github.com/carnager/rofi-pass/blob/master/config.example
    extraConfig = ''
      _rofi () {
        ${config.programs.rofi.package}/bin/rofi -i -no-auto-select "$@"
      }

      URL_field='url'
      USERNAME_field='user'
      AUTOTYPE_field='autotype'
    '';
  };
}
