{ config, pkgs, ... }:

let
  # Scripts for switching between light and dark mode in alacritty
  # its important to have $HOME/.config/alacritty/alacritty-colors.yml in
  # alacritty config imported
  alacrittyLightScript = pkgs.writeShellScriptBin "alacritty-light" ''
    echo '{"colors":{"primary":{"foreground":"#000000","background":"#ffffff"}}}' \
       > ${config.xdg.configHome}/alacritty/alacritty-colors.yml
  '';
  alacrittyDarkScript = pkgs.writeShellScriptBin "alacritty-dark" ''
    echo '{"colors":{"primary":{"foreground":"#f0f0f0","background":"#242424"}}}' \
       > ${config.xdg.configHome}/alacritty/alacritty-colors.yml
  '';

  alacrittyGoTop = pkgs.writeShellScriptBin "gotop.sh" ''
    alacritty -t gotop -e gotop
  '';

  # search files in ~/Downloads and ~/Documents and open it with xdg-open
  # TODO: use xdg-open from pkgs
  fzfFileOpen = pkgs.writeShellScriptBin "fzf-file-open.sh" ''
    xdg-open "$(${pkgs.ripgrep}/bin/rg --no-messages --files --sortr modified ~/Downloads ~/Documents \
      -g "!{*.zip}" | ${config.programs.rofi.package}/bin/rofi -threads 0 -dmenu -sort -sorting-method fzf -i -p "find")"
  '';

  # script for taking screenshots
  takeScreenShot = pkgs.writeShellScriptBin "screenshot.sh" ''
    shottype=$(
       { echo "All Monitors - screen"
         echo "Focused Monitor - output"
         echo "Focused Window - active"
         echo "Select Window - window"
         echo "Area - area"
       } | ${config.programs.rofi.package}/bin/rofi -dmenu -l 5 -p "Take Screenshot")

    type=$(echo $shottype | awk -F'-' '{print $2}' | tr -d " ")
    timestamp=$(date +%Y%m%d%H%M%S)
    ${pkgs.sway-contrib.grimshot}/bin/grimshot --notify save $type ~/Downloads/screenshot-$type-$timestamp.jpg
  '';
in
{
  home.packages = with pkgs; [
    alacrittyLightScript
    alacrittyDarkScript
    alacrittyGoTop
    fzfFileOpen
    takeScreenShot
  ];
}
