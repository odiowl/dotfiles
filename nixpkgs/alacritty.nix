{ pkgs, config, ...}:

{

  enable = true;

  # use simple wrapper around the GL-based alacritty application
  package = pkgs.writeShellScriptBin "alacritty" ''
          ${pkgs.nixgl.nixGLIntel}/bin/nixGLIntel ${pkgs.alacritty}/bin/alacritty "$@"
  '';
  
  settings = {
    import = [
      "${config.xdg.configHome}/alacritty/alacritty-colors.yml"
    ];
    env.TERM = "xterm-256color";
    live_config_reload = true;
    window = {
      padding = {
        x = 2;
        y = 2;
      };
    };
    font = {
      normal = {
        family = "Iosevka Term SS12";
        style = "Regular";
      };
      size = 13.5;
    };
    key_bindings = [
      { key = "Y"; mods = "Control|Shift"; action = "Paste"; }
      { key = "S"; mods = "Control|Shift"; action = "PasteSelection"; }
      { key = "C"; mods = "Control|Shift"; action = "Copy"; }
      { key = "V"; mods = "Alt"; action = "ScrollPageDown"; }
      { key = "V"; mods = "Control"; action = "ScrollPageUp"; }
      { key = "Period"; mods = "Alt|Shift"; action = "ScrollToBottom"; }
      { key = "Comma"; mods = "Alt|Shift"; action = "ScrollToTop"; }
    ];
  };
  
}
