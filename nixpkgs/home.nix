{ config, pkgs, ... }:

# NixOS Stickers: https://www.redbubble.com/de/shop/nixos+stickers

{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "odi";
  home.homeDirectory = "/home/odi";

  # don't show news or notify me about news
  news.display = "silent";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  #home.stateVersion = "22.05";

  programs = {
    # let home manager install and manage itself
    home-manager.enable = true;
    
    jq.enable = true;
    htop.enable = true;

    # Error creating GL context; Could not create EGL display object
    # see: https://github.com/NixOS/nixpkgs/issues/122671
    # therefor I use nixGL as wrapper for alacritty
    alacritty = import ./alacritty.nix { inherit pkgs; inherit config; };

    bash = import ./bash.nix { inherit config; };

    fzf = {
      enable = true;
      enableBashIntegration = true;

      defaultOptions = [
        "--height 100%"
        "--color gutter:-1"
      ];
      
      # historyWidgetOptions will write it's value to ${home.sessionVariables}, see
      # https://github.com/nix-community/home-manager/blob/master/modules/programs/fzf.nix#L136
      # so this works only after logout/login because the sessionVariables are only set after login
      historyWidgetOptions = [
        "--sort"
        "--height 100%"
        "--color gutter:-1"
      ];
    };

    dircolors = {
      enable = true;
      enableBashIntegration = true;
    };

    # keychain = {
    #   enable = true;
    #   enableBashIntegration = true;
    #   agents = [ "gpg" "ssh" ];
    # };

    readline = {
      enable = true;
      # disable case sensitive completion in bash
      extraConfig = ''
        set completion-ignore-case On
      '';
    };

    password-store = {
      enable = true;
      settings = {
        PASSWORD_STORE_DIR="/home/odi/Documents/Passwordstore";
      };
    };

  };

  home.packages = with pkgs; [
    gotop
    pyradio
    # if ripgrep will install during home-configuration it is not available in Emacs!
    ripgrep
    ncdu
    # better to use some special apps from flatpak ... ?
    # I had problems with the cursor theme and the .desktop file
    # update-desktop-database 
    #rocketchat-desktop
    signal-desktop
    pdfarranger
    kubernetes-helm
    openshift
    extra-container
    batsignal
    babashka
    ytfzf
    yt-dlp
  ];

  # TODO: following packages should be migrated to nix
  # - msmtp
  # - mbsync
  # - gnupg
  # - signal -> legacyPackages.x86_64-linux.signal-desktop
  # - firefox
  # - chromium
  # - intellij -> nicht in nixpkgs!
  # - nextcloud
  # - keepassxc

  imports = [
    ./sway.nix
    ./emacs.nix
    ./browsers.nix
    ./git.nix
    ./scripts.nix
    ./desktop-entries.nix
  ];

  #xdg.enable = true;
  #xdg.mime.enable = true;
  #xdg.systemDirs.data = [ "$HOME/.nix-profile/share" ];

  # gtk = {
  #   enable = true;
    
  #   iconTheme = {
  #     name = "Adwaita";
  #     package = pkgs.gnome.adwaita-icon-theme;
  #   };

  #   gtk3.extraConfig = {
  #     gtk-cursor-theme-name = "Adwaita";
  #   };
  #   gtk4.extraConfig = {
  #     gtk-cursor-theme-name = "Adwaita";
  #   };
  # };

  # xsession = {
  #   enable = true;
  #   pointerCursor = {
  #     package = pkgs.gnome.adwaita-icon-theme;
  #     name = "Adwaita";
  #     size = 32;
  #   };
  # };

  # works better with GNU/Linux instead of NixOS
  targets.genericLinux.enable = true;

  # xresources = {
  #   properties = {
  #     "Emacs*toolBar" = 0;
  #   };
  # };

  # TODO: maybe its better to source all config files

  # enable experimental features for new nix package
  xdg.configFile."nix/nix.conf".text = ''
    experimental-features = nix-command flakes
  '';

  # nixpkgs configuration
  nixpkgs.config = import ./nixpkgs-config.nix;
  xdg.configFile."nixpkgs/config.nix".source = ./nixpkgs-config.nix;

  # TODO: I think setting XDG_CURRENT_DESKTOP to only sway make problems
  #       on gnome, this should be tested on gnome
  systemd.user.sessionVariables = {
    # sway:
    #XDG_CURRENT_DESKTOP = "sway:gnome";
    XDG_CURRENT_DESKTOP = "sway";
    MOZ_ENABLE_WAYLAND = 1;
    # wlroots
    WLR_DRM_NO_MODIFIERS = 1;
  };

}
