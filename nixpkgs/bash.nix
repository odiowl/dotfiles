{ config, ... }:
let
  bash.prompt = "";
in
{
  enable = true;

  # important to source nix stuff after installing nix
  # initExtra only runs in interactive shells
  initExtra = ''
    source ${config.home.profileDirectory}/etc/profile.d/nix.sh

    case "$TERM" in
         xterm*|rxvt*|eterm*|tmux*|alacritty*)
            if [ $(id -u) -eq 0 ]; then
               PS1="\[\033[01;31m\] \w #\[\033[00m\] "
            else
               PS1="\[\033[01;32m\] \w \$\[\033[00m\] "
            fi
            ;;
         *)
            ;;
    esac

    export FZF_CTRL_R_OPTS="--height=100% --color gutter:-1"
    stty -ixon # disable Ctrl-s Ctrl-q
    export PASSWORD_STORE_DIR=/home/odi/Documents/Passwordstore
  '';

  # put in $HOME/.profile, will be executed only in login shells
  profileExtra = ''
    export XDG_DATA_DIRS="/home/odi/.nix-profile/share:$XDG_DATA_DIRS"
  '';

  # history
  historyControl = [ "ignoredups" "ignorespace" ];
  historyFileSize = 100000;
  historySize = 10000;
  historyFile = null; # should history file be synced?

  shellAliases = {
    hm = "home-manager --impure";
    hms = "home-manager switch --impure";
    g = "git";
    ls = "ls -hN --color=auto --group-directories-first";
    lfz = "less $(fzf)";
  };

  sessionVariables = {
    FZF_DEFAULT_OPTS = "--height=100%";
  };

  #bashrcExtra = ''
  #'';
  
}
