(use-package erc
  :ensure nil
  
  :custom
  (erc-rename-buffers t)
  (erc-interpret-mirc-color t)
  (erc-lurker-hide-list '("JOIN" "PART" "QUIT"))
  (erc-hide-list '("JOIN" "PART" "QUIT"))
  (erc-autojoin-channels-alist '(("libera.chat" "#emacs" "#systemcrafters")))
  (erc-track-shorten-start 8)
  (erc-track-exclude-server-buffer t)
  (erc-fill-column 100)
  (erc-fill-function 'erc-fill-static)
  (erc-fill-static-center 15)
  (erc-track-exclude '("#emacs"))
  ;; https://github.com/tarsius/minions/issues/22
  (erc-track-position-in-mode-line t))
