;; Base GUI options

;; Use true buttons instead of text with brackets e.g. [Text]
(setq custom-raised-buttons t)

(setq inhibit-startup-message t)

;; Setze das Format des Title des Frames
(setq frame-title-format (format "%s" "Emacs: %b (%m)"))

;; Hier noch eine paar GUI Tools die aktiviert bzw deaktiviert werden
(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode -1)
(show-paren-mode t)
(winner-mode t)
(global-prettify-symbols-mode t)
(column-number-mode t)
(recentf-mode 1)

;; set number of columns; interessting in combination with auto-fill-mode
(setq-default fill-column 95)

;; view the cursor as simple small bar
(setq-default cursor-type 'bar)

;; Fonts
;; (custom-set-faces
;;  '(default ((t (:font "Iosevka Term SS12" :height 123 :weight normal))))
;;  '(fixed-pitch ((t (:family "Iosevka Term SS12" :height 123 :weight normal))))
;;  '(variable-pitch ((t (:family "Cantarell" :height 1.05 :weight normal))))
;;  '(variable-pitch-text ((t :height 1.0 :inherit variable-pitch))))

;; try new fonts
;; fonts are from nano Emacs font stack
;; https://gist.github.com/rougier/b15fb6e98fadb6580958b1733659027b
;; Currently I am not sure if FiraCode or RobotoMono is better for me
(setq od--gui-default-font-height 132)
(setq od--gui-default-font-family-monospace "Iosevka SS12")

(custom-set-faces
 `(default ((t (:font ,od--gui-default-font-family-monospace
		      :height ,od--gui-default-font-height :weight light))))
 `(fixed-pitch ((t (:family ,od--gui-default-font-family-monospace
			    :height ,od--gui-default-font-height :weight light))))
 `(bold ((t (:family ,od--gui-default-font-family-monospace :weight regular))))
 '(variable-pitch ((t (:family "Iosevka Aile" :height 1.0 :weight light))))
 '(variable-pitch-text ((t :inherit variable-pitch))))

(set-fontset-font t 'unicode
    (font-spec :name "Inconsolata Light" :size 16) nil)
(set-fontset-font t '(#xe000 . #xffdd)
    (font-spec :name "Roboto Mono Nerd Font" :size 14) nil)

(defun od--gui-environment-desk ()
  (interactive)
  (let ((font-height 124)
	(font-family "Iosevka Term SS12"))
    (custom-set-faces
     `(default ((t (:font ,font-family :height ,font-height :weight light))))
     `(fixed-pitch ((t (:family ,font-family :height ,font-height :weight light))))
     `(bold ((t (:family ,font-family :weight regular))))
     '(variable-pitch ((t (:family "FiraGO" :height 1.0 :weight light))))
     '(variable-pitch-text ((t :inherit variable-pitch))))

    (set-fontset-font t 'unicode
		      (font-spec :name "Inconsolata Light" :size 16) nil)
    (set-fontset-font t '(#xe000 . #xffdd)
		      (font-spec :name "Fira Code Nerd Font" :size 14) nil)))


(setq selected-frame-dark-p nil)

;; Theming
(use-package modus-themes
  :ensure t
  
  :init
  (setq modus-themes-mode-line '(borderless)
	modus-themes-prompts '(background bold)
	modus-themes-headings '((1 . (medium rainbow 1.3))
				(2 . (medium rainbow 1.2))
				(3 . (medium rainbow 1.15))
				(t . (medium rainbow 1.1)))
	;; this configuration depends havy on the font configuration above
	modus-themes-completions '((matches . (semibold accented intense))
				   (selection . (semibold accented intense))
				   (popup . (semibold accented intense)))
	modus-themes-bold-constructs t
	modus-themes-slanted-constructs nil
	modus-themes-hl-line '(intense)
	modus-themes-region '(bg-only)
	modus-themes-fringes nil
	modus-themes-org-blocks 'greyscale
	modus-themes-scale-headings t
	modus-themes-title 1.1
	modus-themes-lang-checkers '((text-also background))
	modus-themes-paren-match '(bold intense)
	modus-themes-mixed-fonts t
	modus-themes-variable-pitch-ui nil
	modus-themes-org-agenda '((header-block . (1.2 variable-pitch))
				  (header-date . (grayscale bold-today 1.05))
				  (scheduled . rainbow)
				  (habit . nil)
				  (event . (accented varied))))

  ;; Override base colors for modus-vivendi theme that it is more grayisch
  (setq modus-themes-vivendi-color-overrides
	'((bg-main . "gray10")
          (bg-dim . "gray12")
          (bg-alt . "gray13")
          (bg-hl-line . "gray15")
          (bg-active . "gray25")
          (bg-inactive . "gray17")
          (bg-region . "gray20")
          (bg-header . "gray14")
          (bg-tab-bar . "gray20")
          (bg-tab-active . "gray10")
          (bg-tab-inactive . "gray26")
	  (fg-main . "white")
	  (fg-dim . "gray75")
	  (fg-alt . "gray50")
	  (fg-docstring . "gray52")
          (fg-unfocused . "gray72")))
  
  :config
  ;; Lade die helle Variante von Modus Themes per default.
  (load-theme 'modus-operandi t))

;; apply the face on the whole line instead of only the written characters
(setq org-fontify-whole-heading-line t)

(use-package minions
  :config (minions-mode 1))

(defun od--set-selected-frame-dark ()
  "Set GTK theme variant for current frame to DARK."
  (interactive)
  (let ((frame-name (cdr (assoc 'name (frame-parameters)))))
    (call-process-shell-command
     (concat "xprop -f _GTK_THEME_VARIANT 8u -set _GTK_THEME_VARIANT \"dark\" -name \""
	     frame-name
	     "\""))))

(defun od--set-selected-frame-light ()
  "Set GTK theme variant for current frame to LIGHT."
  (interactive)
  (let ((frame-name (cdr (assoc 'name (frame-parameters)))))
    (call-process-shell-command
     (concat "xprop -f _GTK_THEME_VARIANT 8u -set _GTK_THEME_VARIANT \"light\" -name \""
	     frame-name
	     "\""))))

(defun od--themes-dark ()
  "Turn the theme to a dark mode and also turn the GTK frame to the dark variant."
  (interactive)
  (unless selected-frame-dark-p
    (modus-themes-toggle)
    (setq selected-frame-dark-p t)
    (od--set-selected-frame-dark)))

(defun od--themes-light ()
  "Turn the theme to a light mode and also turn the GTK frame to the light variant."
  (interactive)
  (when selected-frame-dark-p
    (modus-themes-toggle)
    (setq selected-frame-dark-p nil)
    (od--set-selected-frame-light)))

(defun od--themes-toggle ()
  "My documentation."
  (interactive)
  (if selected-frame-dark-p
      (od--themes-light)
    (od--themes-dark)))

;; because I use only 29+ version of Emacs
(pixel-scroll-precision-mode 1)

;; Use emojify-mode in all buffers
(use-package emojify
  :init
  (global-emojify-mode)

  :config
  (add-to-list 'emojify-inhibit-major-modes 'org-agenda-mode))
