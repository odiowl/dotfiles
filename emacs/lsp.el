;; Configuration of LSP (language server protocol)

;; https://github.com/emacs-lsp/lsp-mode
(use-package lsp-mode
  :init
  (setq lsp-keymap-prefix "C-c l")
  :hook
  ;; (java-mode . lsp)
  (lsp-mode . lsp-enable-which-key-integration))

(use-package lsp-java
  :init
  (setq lsp-java-java-path "/usr/lib/jvm/java-11-openjdk/bin/java")

  ;; user multiple runtimes
  (setq lsp-java-configuration-runtimes
	'[(:name "JDK-1.8"
		 :path "/home/odi/Libs/jdk1.8.0_202/")
	  (:name "JDK-11"
		 :path "/usr/lib/jvm/java-11-openjdk/"
		 :default t)])

  ;; just use for specific project where we use lombok as well
  (setq lsp-java-vmargs
	(list
	 "-XX:+UseG1GC"
	 "-Xmx1G"
	 "-XX:+UseStringDeduplication"
	 "-javaagent:/home/odi/.m2/repository/org/projectlombok/lombok/1.18.4/lombok-1.18.4.jar")))

;;(use-package company-lsp :ensure t)
;;(use-package lsp-ui :ensure t)


;; DAP ... debug adapter protocol
;; https://github.com/yyoncho/dap-mode
;; debug stuff
;; (use-package dap-mode
;;   :ensure t :after lsp-mode
;;   :config
;;   (dap-mode t)
;;   (dap-ui-mode t))

;; load debug stuff for Java
;;(require 'dap-java)

;; important does not work with java-8!
;;(setq lsp-java-java-path "/usr/lib/jvm/java-11-openjdk-amd64/bin/java")

;; add yasnippets for java
;;(use-package java-snippets)

;; maybe https://github.com/skeeto/javadoc-lookup
