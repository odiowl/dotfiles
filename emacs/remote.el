(setq od--remote-shell-fav-hosts (make-hash-table :test 'equal))

;; some TRAMP configurations
;; got from: https://www.reddit.com/r/emacs/comments/gxhomh/help_tramp_connections_make_emacs_unresponsive_on/
(setq
 ;; Disable version control to avoid delays:
 vc-ignore-dir-regexp (format "\\(%s\\)\\|\\(%s\\)"
			      vc-ignore-dir-regexp tramp-file-name-regexp)
 tramp-copy-size-limit nil
 tramp-default-method "scpx"
 tramp-completion-reread-directory-timeout t)

(setq tramp-ssh-controlmaster-options
      "-o ControlMaster=auto -o ControlPath='tramp.%%C' -o ControlPersist=10")

;; enable/disable tramp debug
;; after enabling see buffer *debug tramp/...
;; (setq tramp-verbose 6)

(defun tramp-abort ()
  (interactive)
  (recentf-cleanup)
  (tramp-cleanup-all-buffers)
  (tramp-cleanup-all-connections))

(defun od--remote-shell-add-fav-host (key tramp-connection)
  (puthash key tramp-connection od--remote-shell-fav-hosts))

(defun od--remote-shell-rm-fav-host (key)
  (remhash key od--remote-shell-fav-hosts))

(defun od--remote-shell (hostname)
  (let ((default-directory hostname))
    (shell (concat "*Remote Shell: " hostname "*"))))

(defun od--remote-vterm (hostname)
  (let ((default-directory hostname))
    (vterm (concat "*vterm: " hostname "*"))))

(defun od--remote-eshell (hostname)
  (let ((default-directory hostname))
    (eshell (concat "*eshell: " hostname "*"))))

(defun od--shell (dirname)
  (let ((default-directory dirname))
    (shell (concat "*Shell: " dirname "*"))))

(defun od--remote-shell-current-directory ()
  (interactive)
  (shell (concat "*Remote Shell: " default-directory "*")))

(defun od--remote-command (hostname command &optional async)
  (let ((default-directory hostname))
    (if async
	(async-shell-command command)
      (shell-command command))))

(defun od--remote-shell-connect (hostname)
  (interactive
   (list (completing-read "Hostname: " (hash-table-keys od--remote-shell-fav-hosts))))
  (od--remote-shell (gethash hostname od--remote-shell-fav-hosts)))

(defun od--remote-vterm-connect (hostname)
  (interactive
   (list (completing-read "Hostname: " (hash-table-keys od--remote-shell-fav-hosts))))
  (od--remote-vterm (gethash hostname od--remote-shell-fav-hosts)))

(defun vterm-ssh (host)
  (vterm)
  (vterm-send-string (concat "ssh " host "\n")))

(defun od--vterm (name)
  (interactive "sBuffername: ")
  (vterm (concat "*vterm: " name "*")))

;; (defun od--sudo-find-file (file)
;;   "Open FILE as root."
;;   (interactive "FOpen file as root: ")
;;   (when (file-writable-p file)
;;     (user-error "File is user writeable, aborting sudo"))
;;   (find-file (if (file-remote-p file)
;; 		 (concat "/" (file-remote-p file 'method) ":"
;; 			 (file-remote-p file 'user) "@" (file-remote-p file 'host)
;; 			 "|sudo:root@"
;; 			 (file-remote-p file 'host) ":" (file-remote-p file 'localname))
;; 	       (concat "/sudo:root@localhost:" file))))

;; (defun sudo-find-file (file-name)
;; "Like find file, but opens the file as root."
;; (interactive "FSudo Find File: ")
;; (let ((tramp-file-name (concat "/sudo::" (expand-file-name file-name))))
;; (find-file tramp-file-name)))
