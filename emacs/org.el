
;; Diary wird der Agenda hinzugefügt
(setq org-agenda-include-diary t)
;; Definiert das Format der Clocksum
(setq org-time-clocksum-format
      '(:hours "%d" :require-hours t :minutes ":%02d" :require-minutes t))
;; org-time-clocksum-format wird nicht mehr unterstützt seit org-mode 9.1
;; daher verwende `org-duration-format'
(setq org-duration-format 'h:mm)
;; Die führenden Zeichen bei Überschriften nicht unterdrücken
(setq org-hide-leading-stars nil)
;; Die Zeichen bei Formatierungen nicht unterdrücken
(setq org-hide-emphasis-markers t)
;; Den aktuellen Clock-Task in der Modeline nicht anzeigen
(setq org-clock-clocked-in-display nil)
;; Logs und Clocks sollen in den DRAWER gelangen
(setq org-log-into-drawer t)
(setq org-clock-into-drawer t)
;; Verwende keine Identation, schaut z.B. nicht gut bei orgzly aus
;; besser `org-ident-mode' verwenden
(setq org-adapt-indentation nil)
;; Loggen wenn ein Task erledigt wurde
(setq org-log-done 'time)

;; Log if a Task will be rescheduled
;; This is also important when I want to query for Tasks which are rescheduled multiple times.
(setq org-log-reschedule 'time)

(setq org-directory "~/Org")
(setq org-directory-gtd (concat org-directory "/GTD"))
(setq org-journal-dir (concat org-directory "/Journals"))
(setq org-projects-directory (concat org-directory "/Projects"))
(setq org-agenda-diary-file (concat org-directory "/Diaries/Diary.org"))

(require 'org-tempo)
(setq org-structure-template-alist
    '(("s" . "src")
      ("e" . "src emacs-lisp")
      ("S" . "src sh")
      ("b" . "src bash")
      ("p" . "src python")
      ("q" . "quote")
      ("v" . "verse")))

(setq org-use-speed-commands t)
(setq org-speed-commands-user
      '(("Outline Visibility")
	("\\" . widen)
	("Outline Structure Editing")
	("y" . org-copy)))

(setq org-refile-use-outline-path 'file
      org-outline-path-complete-in-steps nil
      org-refile-targets
      `(;; in Private.org soll man nur direkt in das File oder in die Projekte refilen können
	(,(concat org-directory-gtd "/Private.org") :level . 0)
	(,(concat org-directory-gtd "/Private.org") :tag . "project")
	(,(concat org-directory-gtd "/Work.org") :level . 0)
	(,(concat org-directory-gtd "/Work.org") :tag . "project")
	(,(concat org-directory "/Links.org") :level . 0)))

(add-hook 'org-mode-hook #'turn-on-auto-fill)

(setq org-agenda-span 'day)

(setq org-agenda-window-setup 'other-window
      org-agenda-restore-windows-after-quit t)

(setq org-agenda-files
      (list org-directory-gtd
	    org-journal-dir
	    (concat org-directory "/Misc/Habits.org")
	    (concat org-directory "/Logs/")       ;; all my memacs logs
	    (concat org-directory "/Links.org")   ;; meine Linksammlung/Bookmarks
	    (concat org-directory "/Diaries/")    ;; alle Kalender
	    (concat org-directory "/Projects/"))) ;; alle meine Projekte

(setq org-todo-keywords
      '((sequence "TODO(t)" "NEXT(n!)" "NOTE(o!)" "|" "DONE(d!)")
	(sequence "WAITING(w@/!)" "SOMEDAY(s@/!)" "|" "CANC(c@/!)")))

(setq org-fontify-done-headline nil
      org-fontify-todo-headline nil)

(setq org-agenda-include-diary t)

(defun org-calendar-holiday-free-time ()
  "Function for showing holidays with free time in diary."
  (require 'holidays)
  (let* ((hl (calendar-check-holidays-for-list calendar-austrian-holidays-free-time
                                               org-agenda-current-date)))
    (and hl (mapconcat #'identity hl "; "))))

(defun org-calendar-holiday-non-free-time ()
  "Function for showing holidays with non free time in diary."
  (require 'holidays)
  (let ((hl (calendar-check-holidays-for-list calendar-austrian-holidays
                                              org-agenda-current-date)))
    (and hl (mapconcat #'identity hl "; "))))

;; create a Property ID on creating items
(setq org-id-link-to-org-use-id 'create-if-interactive)

;; create Property ID for capture items if there isn't currently one
(add-hook 'org-capture-mode-hook #'org-id-get-create)

;; keyboard shortcut im Orgmode damit ich auch nachträglich eine ID vergeben kann
;; TODO: move this shortcut/binding to keys.el
;;(define-key org-mode-map (kbd "C-c m I") #'org-id-get-create)

(setq org-clock-idle-time nil)

(setq org-agenda-clockreport-parameter-plist
      '(:link t :maxlevel 3 :fileskip0 t :compact t :narrow 50! :properties ("CATEGORY")))

(setq org-tags-exclude-from-inheritance '("project")
      org-stuck-projects '("+project/TODO" ("NEXT" "WAITING") nil ""))

;; Orgmode Agendas ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq org-agenda-custom-commands-overview
      '(("o" "Agenda Overview"
	 ((agenda "" ((org-agenda-span 1)))

	  ;; Show Tasks with high priority
	  (org-ql-block '(and (priority "A") (todo) (not (todo "DONE")) (not (todo "CANC")))
			((org-ql-block-header "Important Tasks")))

	  ;; Tasks von Mobile die verschoben werden müssen.
	  (org-ql-block '(and (path "Mobile.org"))
			((org-ql-block-header "Refiling Tasks from Mobile")))

	  ;; Next Tasks
	  ;; Diese Tasks sind die Tasks die als nächstes in einem Projekt
	  ;; anstehen. In einem Projekt sollte es nur einen Task geben der als
	  ;; NEXT gekennzeichnet wurde.
	  (org-ql-block '(and (todo "NEXT") (not (priority "A")) (not (scheduled))
			      (not (tags "email"))
			      (not (tags "call"))
			      (not (tags "chat")))
			((org-ql-block-header "Next Tasks")))

	  ;; Email/Calls and SMS
	  (org-ql-block '(and (todo) (not (priority "A")) (not (scheduled))
			      (or (tags "email") (tags "call") (tags "chat")))
			((org-ql-block-header "Email, Calls, SMS or Chat")))

	  ;; Scheduled within the next 3 days
	  (org-ql-block '(and (todo) (scheduled :from 1 :to 3) (not (habit)))
			((org-ql-block-header "Scheduled next 3 days")))

	  ;; Tasks die mich blockieren weil ich auf etwas oder jemanden warten muß.
	  (org-ql-block '(and (todo "WAITING"))
			((org-ql-block-header "Waiting Tasks")))

	  ;; Kleine Tasks die keinen Bezug zu einem Projekt haben und die keinen Termin
	  ;; hinterlegt haben. Hier werden nur die Tasks angezeigt die in den letzten 15 Tagen eine Aktivität
	  ;; aufweisen können.
	  (org-ql-block '(and (todo "TODO")
			      (not (or (scheduled) (deadline)))
			      (not (children))
			      (not (tags "inbox"))
			      (not (tags "link"))
			      (not (tags "dnf")) ;; Tasks without closing plan
			      (not (or (parent "TODO") (parent (tags "project"))))
			      (not (tags "project"))
			      (not (priority "A"))
			      (or (ts-inactive :from -15) (ts-active :from -15)))
			((org-ql-block-header "Backlog last 15 days")))

	  ;; long time todo's
	  ;; todo's which I do not know when to close it and I use it probably daily
	  (org-ql-block '(and (todo "TODO")
			      (not (or (scheduled) (deadline)))
			      (tags "dnf"))
			((org-ql-block-header "Long Time Todo's")))

	  ;; Aktive Projekte
	  (org-ql-block '(and (todo "TODO") (tags "project") (children (or (todo "NEXT") (todo "WAITING"))))
			((org-ql-block-header "Active Projects")))
	  ;; Stuck projects. A stuck project is a project with the state TODO
	  ;; and do not have child items with state NEXT or state WAITING.
	  (org-ql-block '(and (todo "TODO") (tags "project")
			      (or (not (children))
				  (and (children (todo))
				       (not (children (or (todo "NEXT") (todo "WAITING")))))))
			((org-ql-block-header "Stuck Projects")))

	  ;; Aktuelle Links die ich mir ansehen mag. Ich möchte hier nur die Links sehen die ich in den letzten
	  ;; 30 Tagen angelegt habe. Alle älteren sollen in einem Review-Prozess begutachtet werden.
	  (org-ql-block '(and (todo "TODO") (tags "link")
			      (or (not (tags "mobile")) (not (tags "inbox")))
			      (ts-inactive :from -30))
			((org-ql-block-header "Links")))))))

(setq org-agenda-custom-commands-review
      '(("r" . "Reviews ...")
	("rr" "Refiling Todo's"
	 ((org-ql-block '(and (or (path "Inbox.org") (path "Mobile.org"))
			      (todo))
			((org-ql-block-header "Todo's for refiling")))))
	("rt" "Old Todo List"
	  ((org-ql-block '(and (not (parent (tags "project")))
			       (not (children))
			       (todo "TODO")
			       (not (tags "project"))
			       (not (scheduled))
			       (not (deadline))
			       (not (or (ts-active :from -15) (ts-inactive :from -15))))
			 ((org-ql-block-header "Old Todo List (older than 15 days)")))))
	("rs" "SOMEDAY List"
	 ((org-ql-block '(and (todo "SOMEDAY"))
			((org-ql-block-header "SOMEDAY List")))))
	("ra" "Simple Tasks for Archiving"
	 ((org-ql-block '(and (not (parent (tags "project")))
			     (not (children)) (or (todo "DONE") (todo "CANC")) (not (tags "project"))
			     (not (scheduled)) (not (deadline))
			     (not (or (ts-active :from -30) (ts-inactive :from -30))))
			((org-ql-block-header "Simple Tasks for Archiving (older 30 days)")))))
	("rn" "List of NOTEs for Knowledgebase"
	 ((org-ql-block '(and (todo "NOTE"))
			((org-ql-block-header "Review List of Notes for Knowledgebase")))))))

;; combine all custom-command agenda's
(setq org-agenda-custom-commands (append org-agenda-custom-commands-overview
					 org-agenda-custom-commands-review))

(setq org-agenda-custom-commands-review
      '(("r" "Review"

	 ;; Inboxes

	 ;; Alle Tasks in einen meiner Inboxen, hier muß eine Entscheidung
	 ;; getroffen werden was mit diesen Tasks passieren soll. Diese sollen einfach
	 ;; an die richtige Stelle refiled werden. Die Inboxen sollten immer geleert werden.
	 ((org-ql-block '(and (or (path "Inbox.org") (path "Mobile.org")))
			((org-ql-block-header "INBOXes Refiling")))

	  ;; Tasks ohne Tags
	  ;; Manchmal lege ich mir schnell einen Task an ohne wirklich zu überlegen welchen Tag
	  ;; dieser bekommen soll. Hier sollen alle Tasks angezeigt werden wo ich mir noch überlegen
	  ;; sollte welchen Tag dieser bekommt.
	  ;; Ausgenommen sind Projekte, hier hab ich oft den Fall das einzelne Tasks keinen Tag
	  ;; besitzen. Durch das Projekt sind sie aber schon gut definiert.
	  (org-ql-block '(and (todo "TODO")
			      (not (tags-local))
			      (not (parent (tags-local "project")))
			      (not (tags-inherited "link"))
			      (not (or (scheduled) (deadline))))
			((org-ql-block-header "Tasks without Local Tags and not scheduled")))

	  ;; Projects

	  ;; Stucked Projects
	  ;; Ein Stucked Project ist ein Projekt das entweder keine Tasks beinhaltet alle Tasks
	  ;; abgeschlossen wurden oder keinen Task im Status NEXT hat.
	  (org-ql-block '(and (todo "TODO") (tags "project") (not (children (todo "NEXT"))))
			((org-ql-block-header "Stuck Projects")))

	  ;; Todos in Stucked Projekten
	  ;; Hier muß man einen Task pro Projekt als NEXT Task definieren.
	  (org-ql-block '(and (parent (and (todo "TODO") (tags "project"))) (not (parent (children (todo "NEXT")))) (todo "TODO"))
			((org-ql-block-header "Open Tasks in active Stuck Projects")))

	  ;; Projekte die abgeschlossen sind, sprich alle Subtasks sind abgeschlossen (DONE,CANC) und
	  ;; das Projekt selbst ist auch als abgeschlossen (DONE,CANC) markiert.
	  ;; Bevor das Projekt archiviert wird, sollte man noch einmal durchsehen ob etwas in die
	  ;; Knowledgebase wandert sollte.
	  (org-ql-block '(and (tags "project")
			      (or (todo "DONE") (todo "CANC"))
			      (children (or (todo "DONE") (todo "CANC"))))
			((org-ql-block-header "Project for Archiving")))

	  ;; Single Tasks

	  ;; Alle Todo-Items in Private und Work die zwar Todo's sind, jedoch
	  ;; aber (noch) nicht geplant (SCHEDULED) sind und in keinem Projekt hängen.
	  ;; Hier gibt es zwei Entscheidungen:
	  ;;   - Task erledigen und abschließen
	  ;;   - Task schedulen damit dieser später wieder aufpoppt
	  (org-ql-block '(and (or (path "Private.org") (path "Work.org"))
			      (not (parent (tags "project")))
			      (not (children))
			      (todo "TODO")
			      (not (tags "project"))
			      (not (scheduled))
			      (not (deadline))
			      (not (or (ts-active :from -15) (ts-inactive :from -15))))
			((org-ql-block-header "Simple Open Tasks (older than 15 days)")))

	  ;; Alle erledigten Tasks in Private und Work die archiviert werden sollen.
	  ;; Diese Tasks wurden nicht geplant (SCHEDULED,DEADLINE) und sind in keinem Projekt.
	  ;; Grundsätzlich werden diese Tasks alle archiviert. Zuvor sollten man noch einmal durchsehen
	  ;; ob innerhalb dieses Tasks Informationen enthalten sind die in die Knowledgebase gehören.
	  (org-ql-block '(and (or (path "Private.org") (path "Work.org")) (not (parent (tags "project")))
			      (not (children)) (or (todo "DONE") (todo "CANC")) (not (tags "project"))
			      (not (scheduled)) (not (deadline)))
			((org-ql-block-header "Simple Tasks for Archiving")))

	  ;; Alle abgeschlossenen Tasks die eine Notiz beinhalten. Hier muß ich bevor
	  ;; ich den Task lösche schauen ob ich die Notiz noch irgendwie brauchen kann.
	  ;; Wenn ja kann die Information in mein Wiki wandern.
	  ;; (org-ql-block '(and (or (path "Private.org") (path "Work.org"))
	  ;; 		      (not (parent (tags "project")))
	  ;; 		      (not (children))
	  ;; 		      (or (todo "DONE") (todo "CANC"))
	  ;; 		      (regexp "Note taken"))
	  ;; 		((org-ql-block-header "Closed Tasks without Project and a Note")))

	  ;; Tasks die erledigt wurden, selbst aber kein Projekt sind (not (children))
	  ;; und auch in keinem Projekt hängen. Diese Liste stellt lediglich Tasks dar
	  ;; die einzeln zu betrachten sind. Diese Tasks können normalerweise archiviert
	  ;; werden weil sie nicht mehr gebrauch werden. Es werden nur Tasks angezeigt
	  ;; die älter als 10 Tage sind. Eventuell überschneidet sich diese Abfrage
	  ;; mit der obigen und ich brauche die obige nicht mehr. Hier muß man nachsehen
	  ;; ob noch eine Information benötigt wird.
	  ;; (org-ql-block '(and (or (path "Private.org") (path "Work.org")) (not (parent (tags "project")))
	  ;; 		      (or (todo "DONE") (todo "CANC")) (not (children)) (not (clocked)) (closed :to -10))
	  ;; 		((org-ql-block-header "Single closed and not clocked Tasks - Older than 10 days")))

	  ;; Alle abgeschlossenen einzelne Tasks die eine CLOCK beinhalten. Diese werden erst nach
	  ;; 40 Tagen angezeigt damit ich keine Probleme beim Clockreporting habe. Diese können dann
	  ;; ganz einfach archiviert werden.
	  ;; (org-ql-block '(and (or (path "Private.org") (path "Work.org")) (not (parent (tags "project")))
	  ;; 		      (or (todo "DONE") (todo "CANC")) (not (children)) (clocked) (ts-inactive :to -40))
	  ;; 		((org-ql-block-header "Single closed and clocked Tasks - Older than 40 days")))

	  ;; Rescheduled Tasks

	  ;; Tasks die zumindest schon einmal neu scheduled wurden.
	  (org-ql-block '(and (not (done)) (regexp "- Rescheduled from"))
			((org-ql-block-header "Rescheduled Tasks")))	  

	  ;; Links

	  ;; Alte Links die älter als 30 Tage sind.
	  (org-ql-block '(and (todo "TODO") (tags "link") (ts-inactive :to -30))
			((org-ql-block-header "Old Links")))

	  ;; Someday Liste
	  (org-ql-block '(and (todo "SOMEDAY"))
			((org-ql-block-header "Someday")))))))

(setq org-agenda-custom-commands-reviews-links
      '("rl" "Links/Bookmarks"
	((org-ql-block '(and (todo "TODO") (tags "link") (ts-inactive :from -30))
		       ((org-ql-block-header "Open Links")))
	 (org-ql-block '(and (todo "TODO") (tags "link") (ts-inactive :to -30))
		       ((org-ql-block-header "Old Links"))))))

;; combine all custom-command agenda's
(setq org-agenda-custom-commands (append org-agenda-custom-commands-overview
					 org-agenda-custom-commands-review))

;; After jumping to a subtree in an orgmode file from the agenda (`org-agenda-goto'), don't
;; show all other items from the file, only the subtree.
(advice-add 'org-agenda-goto :after
	    (lambda (&rest args)
	      (org-narrow-to-subtree)))

;; Orgmode Capture Templates ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; be sure that there is no entry in the templates list
(setq org-capture-templates nil)

(add-to-list 'org-capture-templates
	     `("p" "New Private Todo Entry" entry
	       (file ,(concat org-directory-gtd "/Private.org"))
	       ,(concat
		 "* TODO %^{Title}\n"
		 ":PROPERTIES:\n"
		 ":CREATED:  [%<%Y-%m-%d %a %H:%M>]\n"
		 ":END:\n%?") :prepand nil))

(add-to-list 'org-capture-templates
	     `("w" "New Work Todo Entry" entry
	       (file ,(concat org-directory-gtd "/Work.org"))
	       ,(concat
		 "* TODO %^{Title}\n"
		 ":PROPERTIES:\n"
		 ":CREATED:  [%<%Y-%m-%d %a %H:%M>]\n"
		 ":END:\n%?") :prepand nil))

(add-to-list 'org-capture-templates
	     `("j" "Journal Entry" entry
	       (file+datetree ,(concat org-directory "/Journals/" (format-time-string "%Y-%m") ".org"))
	       "* %?\n" :prepand nil))

(add-to-list 'org-capture-templates
             `("m" "Meeting Notes" entry
               (file ,(concat org-directory-gtd "/Work.org"))
               ,(concat
                 "* %^{Title} :meet:\n"
                 ":PROPERTIES:\n"
                 ":CREATED:  [%<%Y-%m-%d %a %H:%M>]\n"
                 ":END:\n  %?")
               :clock-in t))

(add-to-list 'org-capture-templates
	     `("e" "New Todo Entry with Email" entry
	       (file ,(concat org-directory-gtd "/Inbox.org"))
	       ,(concat
		 "* TODO %a :email:\n"
		 ":PROPERTIES:\n"
		 ":CREATED:  [%<%Y-%m-%d %a %H:%M>]\n"
		 ":END:\n  %?") :prepand nil))

(add-to-list 'org-capture-templates
	     `("l" "Add new Bookmark/Link" entry
	       (file ,(concat org-directory "/Links.org"))
	       ,(concat
		 "* %^{Title}\n"
		 ":PROPERTIES:\n"
		 ":CREATED:  [%<%Y-%m-%d %a %H:%M>]\n"
		 ":URL:      %c\n"
		 ":END:\n%?")))

;; Babel Languages ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; define languages for babel
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (clojure . t)
   (restclient . t)
   (shell . t)
   (dot . t)))

;; Notifications ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq alert-default-style 'notifications)

(defun my-appt-disp-function (min-to-app new-time appt-msg)
  (appt-disp-window min-to-app new-time appt-msg))
  ;; (alert (concat "Appt in " min-to-app " minutes")
  ;; 	 :title (concat new-time appt-msg)
  ;; 	 :severity 'normal))

(setq appt-disp-window-function 'my-appt-disp-function)

(add-hook 'org-agenda-finalize-hook 'org-agenda-to-appt)

;; Functions for my journals ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq od--org-journals-directory (concat org-directory "/Journals"))

(defun od--org-goto-datetree (&optional select)
  "Goto Journal Header of the current Journal File. If SELECT is not nil ask for date."
  (let* ((filename (format-time-string "%Y-%m"))
	 (file (concat od--org-journals-directory "/" filename ".org"))
	 (heading (format-time-string "%A %d %b")))
    (when select
      (let ((ts (apply 'encode-time (org-parse-time-string (org-read-date)) nil)))
	(setq filename (format-time-string "%Y-%m" ts))
	(setq file (concat od--org-journals-directory "/" filename ".org"))
	(setq heading (format-time-string "%A %d %b" ts))))
    (find-file file)
    (goto-char 0)
    (unless (search-forward (format "* %s" heading) nil t)
      (goto-line (point-max))
      (insert (format "* %s" heading)))))

(defun od--org-goto-journal-entry ()
  (interactive)
  (if (equal current-prefix-arg nil)
      (od--org-goto-datetree)
    (od--org-goto-datetree t)))

(defun od--org-goto-datetree-with-select ()
  (od--org-goto-datetree t))

(defun od--org-goto-journal-today ()
  (interactive)
  (od--org-goto-datetree))

(add-to-list 'org-capture-templates
	     `("j" "Journal Entry" entry
	       (function od--org-goto-datetree-with-select)
	       ,(concat
		 "* %?")
	       :empty-lines 0 :jump-to-captured t))

(use-package org-journal
  :ensure t
  :defer t
  :init
  ;; Change default prefix key; needs to be set before loading org-journal
  (setq org-journal-prefix-key "C-c j ")
  :config
  (setq org-journal-dir (concat org-directory "/Journals/")
        org-journal-date-format "%A, %d %B %Y"
	org-journal-file-format "%Y-%m.org"
	org-journal-created-property-timestamp-format "[%Y-%m-%d %a]"
	org-journal-file-type 'monthly
	org-journal-enable-agenda-integration t
	org-journal-enable-cache t
	org-journal-follow-mode t
	org-journal-carryover-items ""))

;; Contacts ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (use-package org-contacts
;;   :ensure nil
;;   :custom
;;   (org-contacts-files `(,(concat org-directory "/Misc/Contacts.org")))
;;   (org-contacts-icon-use-gravatar nil))

;; Calendar synchronization with Nextcloud ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; https://github.com/dengste/org-caldav
;;
;; I use this package for synchronize calendar events from Nextcloud calendar mostly for
;; readiing purpose.
;;
;; TODO: add username/password to authinfo or better use keepass for that
(use-package org-caldav
  :ensure t
  
  :init
  (setq org-icalendar-timezone "Europe/Vienna"
	org-caldav-delete-calendar-entries 'never
	org-caldav-delete-org-entries 'ask
	org-caldav-files nil
	org-caldav-url (concat (od--auth-get-field "nextcloud" :url) "remote.php/dav/calendars/Oliver"))

  :config
  (setq org-caldav-calendars
	`((:calendar-id "oliver" :inbox ,(concat org-directory "/Diaries/Oliver.org"))
	  (:calendar-id "personal_shared_by_Sandra" :inbox ,(concat org-directory "/Diaries/Sandra.org"))
	  (:calendar-id "emil" :inbox ,(concat org-directory "/Diaries/Emil.org"))
	  (:calendar-id "emma" :inbox ,(concat org-directory "/Diaries/Emma.org"))
	  (:calendar-id "family" :inbox ,(concat org-directory "/Diaries/Family.org"))
	  (:calendar-id "house" :inbox ,(concat org-directory "/Diaries/House.org")))))

;; Restclient for Orgmode ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package ob-restclient
  :ensure t)

;; Org Roam ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package org-roam
 :init
 (setq org-roam-v2-ack t)

 :custom
 (org-roam-directory (concat org-directory "/Roam"))

 :bind (("C-c n f" . org-roam-node-find)
	("C-c n i" . org-roam-node-insert))

 :config
 (org-roam-db-autosync-mode))
