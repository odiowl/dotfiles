(require 'elfeed)
(require 'elfeed-link)

;; RSS Feeds ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq elfeed-feeds
      '(;; news
	("https://futurezone.at/xml/rss" news tech)
	("http://comicfeeds.chrisbenard.net/view/dilbert/default" dilbert)
	;; emacs
	("https://planet.emacslife.com/atom.xml" emacs planet)
	;; lisp, clojure, ...
	("http://planet.clojure.in/atom.xml" lisp clojure)
	("http://planet.lisp.org/rss20.xml" lisp)
	("https://todayinclojure.com/rss" lisp clojure)
	;; fedora
	;; gnome
	("http://planet.gnome.org/atom.xml" gnome linux)
	;; youtube
	("https://www.youtube.com/feeds/videos.xml?channel_id=UC2eYFnH61tmytImy1mTYvhA" youtube)
	("https://www.youtube.com/feeds/videos.xml?channel_id=UC0uTPqBCFIpZxlz_Lv1tk_g" youtube)
	("https://www.youtube.com/feeds/videos.xml?channel_id=UCVls1GmFKf6WlTraIb_IaJg" youtube)
	;; linux
	("https://linuxnews.de/feed/" linux)
	;; personal feeds
        ("http://newartisans.com/rss.xml" emacs blog))
      elfeed-sort-order 'ascending)

;; If the default browser is e.g. w3m it is sometimes nice to open some feeds
;; in an external browser. This function opens the current message in the
;; firefox browser.
(defun elfeed-show-visit-external ()
  "Visit the current entry in an external browser.
This function uses `ask-browse-url' for calling the link.  It opens the link in
the default browser, with a prefix command a selection menu for all defined
browsers will be shown."
  (interactive)
  (let ((link (elfeed-entry-link elfeed-show-entry)))
    (when link
      (ask-browse-url link))))

(define-key elfeed-show-mode-map (kbd "B") 'elfeed-show-visit-external)

;; Function for open the URL in the feed with `eww'. EWW is nice because it has a
;; `readable-mode' with the keybinding 'R'.
(defun elfeed-show-visit-eww ()
  (interactive)
  (let ((link (elfeed-entry-link elfeed-show-entry)))
    (when link
      (eww-browse-url link))))

(define-key elfeed-show-mode-map (kbd "e") 'elfeed-show-visit-eww)

;; TODO: Do I need a Hydra for elfeed? Maybe with the common keybindings
;;       C-c m m?

;; ;; hydra for elfeed
;; (defhydra od/hydra-elfeed (:color red :hint nil :columns 4)
;;   " Elfeed filters "
;;   ("G" elfeed-search-fetch "update")
;;   ("tt" (elfeed-search-set-filter "@1-day-ago") "Today all")
;;   ("tu" (elfeed-search-set-filter "@1-day-ago +unread") "Today unread")
;;   ("N" (elfeed-search-set-filter "@6-month-ago +news +unread") "News")
;;   ("ee" (elfeed-search-set-filter "@6-month-ago +emacs +unread") "Emacs")
;;   ("es" (elfeed-search-set-filter "@6-month-ago +emacs +unread +sx") "Emacs (StackExchange)")
;;   ("er" (elfeed-search-set-filter "@6-month-ago +emacs +unread +reddit") "Emacs (Reddit)")
;;   ("ll" (elfeed-search-set-filter "@6-month-ago +lisp +unread") "Lisps")
;;   ("lc" (elfeed-search-set-filter "@6-month-ago +lisp +unread +clojure") "Lisps (Clojure)")
;;   ("P" (elfeed-search-set-filter "@6-month-ago +podcast +unread") "Podcasts")
;;   ("d" (elfeed-search-set-filter "@6-month-ago +dilbert +unread") "Dilbert")
;;   ("U" (elfeed-search-set-filter "+unread") "All Unread")
;;   ("b" (elfeed-search-set-filter "+bmark") "Bookmarked")
;;   ("g" (elfeed-search-set-filter "+unread +gnome") "Gnome")
;;   ("y" (elfeed-search-set-filter "+unread +youtube"))
;;   ("A" (elfeed-search-set-filter "") "all")
;;   ("S" elfeed-search-show-entry "Show entry")
;;   ("n" next-line "next")
;;   ("p" previous-line "previous")
;;   ("q" nil "quit"))

;; (define-key elfeed-search-mode-map (kbd "f") 'od/hydra-elfeed/body)

;; Local Variables:
;; eval: (flycheck-mode -1)
;; End:
;;; elfeed.el ends here

