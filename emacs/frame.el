;;; package --- This package gives my some useful functions for working with frames.
;;; Commentary:
;;; Code:

;; FIXME: Does not work on my internal monitor if it is the secondary
;;        monitor. After some tests I figured out that the problem is
;;        the top bar. If we have a bar on the top it works right!
(defun od--current-frame-center ()
  "Center the current selected Emacs frame."
  (interactive)
  (let* ((geo (assoc 'geometry (frame-monitor-attributes)))
	 (max-width (nth 3 geo))
	 (max-height (nth 4 geo))
	 (width (floor (* max-width 0.65)))
	 (height (floor (* max-height 0.95)))
	 (x-offset (nth 1 geo))
	 (y-offset 20))
    (set-frame-position (selected-frame)
			;; calculate left starting point
			(+ (/ (- max-width width) 2) x-offset)
			;; calculate top starting point
			(+ (/ (- max-height height) 3) y-offset))
    (set-frame-size (selected-frame) width height t)))

;; NOTE: Works fine but has the same problem with top bar.
(defun od--current-frame-move-right-with-border ()
  "Move frame to the right of the monitor with little border."
  (interactive)
  (let* ((geo (assoc 'geometry (frame-monitor-attributes)))
	 (max-width (nth 3 geo))
	 (max-height (nth 4 geo))
	 (width (floor (* max-width 0.47)))
	 (height (floor (* max-height 0.93)))
	 (x-offset (nth 1 geo)))
    (set-frame-size (selected-frame) width height t)
    (set-frame-position (selected-frame) (- (+ max-width x-offset) width 45) 40)))

;; NOTE: Works fine but has the same problem with top bar.
(defun od--current-frame-move-left-with-border ()
  "Move frame to the left of the monitor with little border."
  (interactive)
  (let* ((geo (assoc 'geometry (frame-monitor-attributes)))
	 (max-width (nth 3 geo))
	 (max-height (nth 4 geo))
	 (width (floor (* max-width 0.47)))
	 (height (floor (* max-height 0.93)))
	 (x-offset (nth 1 geo)))
    (set-frame-size (selected-frame) width height t)
    (set-frame-position (selected-frame) (+ x-offset 25) 40)))

(defun od--current-frame-resize-small ()
  (interactive)
  (set-frame-size (selected-frame) 870 980 t))

(defun od--current-frame-resize-large ()
  (interactive)
  (set-frame-size (selected-frame) 1050 1250 t))

;; Local Variables:
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
;;; frame.el ends here
