;;; package --- This descripes my mode-map, simmilar to spacemacs/doom-emacs
;;; Commentary:
;;; Code:

(use-package which-key
  :ensure
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.3)
  (which-key-mode))

;; define my own keymaps
(define-prefix-command 'od-map)

;; define prefix key for my key-map, I use the <menu> key for this
;; I have remapped Caps-Lock as additional menu key in Gnome
(setq od/which-key-prefix-key "C-c m")

(defmacro define-and-name-key (keys name f)
  "Define a macro for naming key combination and bind them to a function.
The function F will bind to the key combination KEYS and will produce a
NAME for which-key."
  `(progn
     (which-key-add-key-based-replacements
       (concat od/which-key-prefix-key " " ,keys) ,name)
     (define-key od-map (kbd ,keys) ,f)))

(defmacro define-and-name-key-with-local-map (keys name f)
  "Define a macro for naming key combination and bind them to a function.
The function F will bind to the key combination KEYS and will produce a
NAME for which-key.  This key binding works only on the `current-local-map'."
  `(progn
     (which-key-add-key-based-replacements
       (concat od/which-key-prefix-key " " ,keys) ,name)
     (define-key (current-local-map) (kbd (concat od/which-key-prefix-key " " ,keys)) ,f)))

;; shortcut for starting my keymap -- leader key
(global-set-key (kbd od/which-key-prefix-key) 'od-map)

;; Frame commands ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(which-key-add-key-based-replacements
  (concat od/which-key-prefix-key " " "f") "Frame")
(define-and-name-key "f c" "create new Frame" #'make-frame-command)
(define-and-name-key "f k" "kill Frame" #'delete-frame)
(define-and-name-key "f o" "switch to other Frame" #'other-frame)
(define-and-name-key "f s" "resize frame to small" #'od--current-frame-resize-small)
(define-and-name-key "f l" "resize frame to large" #'od--current-frame-resize-large)

;; Search commands
(which-key-add-key-based-replacements
  (concat od/which-key-prefix-key " " "s") "Search")
(define-and-name-key "s r" "Consult Ripgrep" #'consult-ripgrep)
(define-and-name-key "s l" "Consult Line" #'consult-line)
(define-and-name-key "s m" "Mu4e Search" #'mu4e-headers-search)

;; Terminal commands
(which-key-add-key-based-replacements
  (concat od/which-key-prefix-key " " "t") "Terminal")
(define-and-name-key "t t" "New or Switch to VTerm" #'vterm)
(define-and-name-key "t n" "New VTerm" #'(lambda () (interactive) (vterm t)))

;; GUI commands ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(which-key-add-key-based-replacements
  (concat od/which-key-prefix-key " " "g") "GUI")
(define-and-name-key "g t" "toggle light/dark theme" #'od--themes-toggle)

;; Main commands ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define-and-name-key "M" "Mu4e" 'mu4e)
(define-and-name-key "E" "Elfeed" 'elfeed)
(define-and-name-key "I" "ERC" #'od--erc-start-or-switch)

;; Global Keybindings ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
(global-set-key (kbd "C-x b") #'consult-buffer)
(global-set-key (kbd "<f5>") 'modus-themes-toggle)
(global-set-key (kbd "<f11>") #'make-frame-command)
(global-set-key (kbd "<f12>") #'delete-frame)
(global-set-key (kbd "M-s r") #'consult-ripgrep)

;; Add some Keybindings to some modes ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define-key dired-mode-map (kbd "X") 'od--remote-shell-current-directory)
(define-key dired-mode-map (kbd "E") 'eshell)
(define-key dired-mode-map (kbd "V") 'vterm)

;; Local Variables:
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
;;; keys.el ends here

